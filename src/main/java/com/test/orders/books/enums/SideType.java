package com.test.orders.books.enums;

public enum SideType {
	BID('B'), OFFER('O');

	private char value;

	private SideType(char value) {
		this.value = value;
	}

	public char toValue() {
		return value;
	}

	public static boolean isValid(char value) {
		boolean found = false;
		for (SideType st : SideType.values()) {
			if (st.toValue() == value) {
				return true;
			}
		}
		return found;
	}
}
