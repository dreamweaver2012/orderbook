package com.test.orders.books;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.aol.cyclops.streams.StreamUtils;
import com.test.orders.books.enums.SideType;
import com.test.orders.books.exceptions.InvalidParameterException;

/** 
 * Class that offers the implementation of the requirements.
 * 
 * @author Dragos Manea
 *
 */
public class OrderBook implements OrderBookContract {

	// LinkedHashSet
	private LinkedHashMap<Long, Order> ordersRegistry;
	private Map<String, LinkedHashSet<Long>> priceMap;
	private List<Double> bidPricesOrdered;
	private List<Double> offerPricesOrdered;

	public OrderBook() {
		ordersRegistry = new LinkedHashMap<Long, Order>();
		priceMap = new ConcurrentHashMap<String, LinkedHashSet<Long>>();
		bidPricesOrdered = new ArrayList<Double>();
		offerPricesOrdered = new ArrayList<Double>();
	}

	/**
	 * Adds an order to the OrderBook
	 * 
	 * @throws @InvalidParameterException if the specified order's side is invalid
	 */
	public synchronized void addOrder(Order order) throws InvalidParameterException {
		if (!SideType.isValid(order.getSide())) {
			throw new InvalidParameterException("side", "Invalid order's side");
		}
		ordersRegistry.put(order.getId(), order);
		String key = "" + order.getSide() + order.getPrice();
		LinkedHashSet<Long> samePriceOffers = priceMap.get(key);
		if (samePriceOffers == null) {
			samePriceOffers = new LinkedHashSet<Long>();
			if (order.getSide() == SideType.BID.toValue()) {
				int idx = Collections.binarySearch(bidPricesOrdered, order.getPrice());
				if (idx < 0) {
					bidPricesOrdered.add(-idx - 1, order.getPrice());
				}
			} else if (order.getSide() == SideType.OFFER.toValue()) {
				int idx = Collections.binarySearch(offerPricesOrdered, order.getPrice());
				if (idx < 0) {
					offerPricesOrdered.add(-idx - 1, order.getPrice());
				}
			}
		}
		samePriceOffers.add(order.getId());
		priceMap.put(key, samePriceOffers);
	}

	/**
	 * Removes the specified Order (by Id) from the OrderBook
	 * 
	 * @throws @InvalidParameterException if the specified order ID doesn't exist
	 */
	public synchronized void removeOrderById(long orderId) throws InvalidParameterException {
		Order o = ordersRegistry.get(orderId);
		if (o == null) {
			throw new InvalidParameterException("orderId", "Non-existing order");
		}

		LinkedHashSet<Long> existingPriceList = priceMap.get("" + o.getSide() + o.getPrice());
		if (existingPriceList.size() > 1) {
			existingPriceList.remove(o.getId());
		} else {
			priceMap.remove("" + o.getSide() + o.getPrice());
			if (o.getSide() == SideType.BID.toValue()) {
				bidPricesOrdered.remove(Collections.binarySearch(bidPricesOrdered, o.getPrice()));
			} else if (o.getSide() == SideType.OFFER.toValue()) {
				offerPricesOrdered.remove(Collections.binarySearch(offerPricesOrdered, o.getPrice()));
			}
		}
		ordersRegistry.remove(orderId);
	}

	/**
	 * Modifies an existing order in the book to use the new size
	 * 
	 * @throws @InvalidParameterException if the specified order ID doesn't exist
	 */
	public synchronized void updateOrderSize(long orderId, long size) throws InvalidParameterException {
		Order existingOrder = ordersRegistry.get(orderId);
		if (existingOrder != null) {
			existingOrder.setSize(size);
		} else {
			throw new InvalidParameterException("orderId", "Non-existing order");
		}
	}

	/**
	 * @return - The price for the specified side and level
	 * @throws @InvalidParameterException if the specified side or level are invalid
	 */
	public Double getLevelPrice(char side, int level) throws InvalidParameterException {
		if (side == SideType.BID.toValue()) {
			if (level >= 0 && level <= bidPricesOrdered.size()) {
				return bidPricesOrdered.get(bidPricesOrdered.size() - level);
			} else {
				throw new InvalidParameterException("level", "Level out of bounds");
			}
		} else if (side == SideType.OFFER.toValue()) {
			if (level >= 0 && level <= offerPricesOrdered.size()) {
				return offerPricesOrdered.get(level - 1);
			} else {
				throw new InvalidParameterException("level", "Level out of bounds");
			}
		}
		throw new InvalidParameterException("side", "Invalid side");
	}

	/**
	 * @return - The total size for the specified side and level
	 * @throws @InvalidParameterException if the specified side or level are invalid
	 */
	public Long getLevelTotalSize(char side, int level) throws InvalidParameterException {
		if (!SideType.isValid(side)) {
			throw new InvalidParameterException("side", "Invalid side");
		}
		return priceMap.get("" + side + getLevelPrice(side, level)).stream().map(i -> ordersRegistry.get(i))
				.mapToLong(Order::getSize).sum();
	}

	/**
	 * @return - All the orders from the specified side of the book, in level- and
	 *         time-order
	 * 
	 * @throws @InvalidParameterException if the specified side is invalid
	 */
	public List<Order> getAllOrdersBySide(char side) throws InvalidParameterException {
		Stream<Double> pricesStream;
		if (side == SideType.BID.toValue()) {
			pricesStream = StreamUtils.reversedStream(bidPricesOrdered);
		} else if (side == SideType.OFFER.toValue()) {
			pricesStream = offerPricesOrdered.stream();
		} else {
			throw new InvalidParameterException("side", "Invalid side");
		}
		return pricesStream.map(price -> priceMap.get("" + side + price)).flatMap(Collection::stream)
				.map(id -> ordersRegistry.get(id)).collect(Collectors.toList());
	}

	public Order retrieveOrder(long orderId) {
		return ordersRegistry.get(orderId);
	}

}
