package com.test.orders.books.exceptions;

/**
 * Class used to signal that invalid parameters have been provided.
 * 
 * @author Dragos Manea
 *
 */
public class InvalidParameterException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String message;
	private String parameterName;

	InvalidParameterException() {
		super();
	}

	public InvalidParameterException(String message) {
		super(message);
		this.message = message;
	}

	public InvalidParameterException(String parameterName, String message) {
		super(message);
		this.parameterName = parameterName;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	/*
	 * public void setMessage(String message) { this.message = message; }
	 */

	public String getParameterName() {
		return parameterName;
	}

	/*
	 * public void setParameterName(String parameterName) { this.parameterName =
	 * parameterName; }
	 */

}
