package com.test.orders.books;

import java.util.List;

import com.test.orders.books.exceptions.InvalidParameterException;

/**
 * Interface that specifies the methods to be implemented as requirements.
 * 
 * @author Dragos Manea
 *
 */
public interface OrderBookContract {

	void addOrder(Order order) throws InvalidParameterException;

	void removeOrderById(long orderId) throws InvalidParameterException;

	void updateOrderSize(long orderId, long size) throws InvalidParameterException;

	Double getLevelPrice(char side, int level) throws InvalidParameterException;

	Long getLevelTotalSize(char side, int level) throws InvalidParameterException;

	List<Order> getAllOrdersBySide(char side) throws InvalidParameterException;
}
