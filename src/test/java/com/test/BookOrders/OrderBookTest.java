package com.test.BookOrders;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.test.orders.books.Order;
import com.test.orders.books.OrderBook;

public class OrderBookTest {

	@Test
	public void test_addOrder_when_order_provided_shouldBeRetrievable() {
		OrderBook ob = new OrderBook();
		Order o1 = new Order(1, 10, 'B', 11);
		ob.addOrder(o1);
		Order retrievedOrder = ob.retrieveOrder(1);
		Assertions.assertNotNull(retrievedOrder);
		Assertions.assertEquals(o1, retrievedOrder);
	}

	@Test
	public void test_addOrder_when_given_invalid_order_side_shouldThrowException() {
		OrderBook ob = new OrderBook();
		Order o1 = new Order(1, 10, 'X', 11);
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.addOrder(o1);
		});
	}

	@Test
	public void test_removeOrderById_when_given_invalid_id_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.removeOrderById(1000);
		});
	}

	@Test
	public void test_removeOrderById_when_given_valid_id_order_should_not_be_retrievable() {
		OrderBook ob = new OrderBook();
		Order o1 = new Order(1, 10, 'B', 11);
		Order o2 = new Order(2, 20, 'O', 22);
		Order o3 = new Order(3, 20, 'O', 55);
		ob.addOrder(o1);
		ob.addOrder(o2);
		ob.addOrder(o3);
		Assertions.assertDoesNotThrow(() -> {
			ob.removeOrderById(1);
			ob.removeOrderById(2);
			ob.removeOrderById(3);
		});
		Order retrievedOrder1 = ob.retrieveOrder(1);
		Assertions.assertNull(retrievedOrder1);
		Order retrievedOrder2 = ob.retrieveOrder(2);
		Assertions.assertNull(retrievedOrder2);
		Order retrievedOrder3 = ob.retrieveOrder(3);
		Assertions.assertNull(retrievedOrder3);
	}

	@Test
	public void test_updateOrderSize_when_given_invalid_id_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.updateOrderSize(1000, 1000);
		});
	}

	@Test
	public void test_updateOrderSize_when_update_size_shouldRetrieveNewValue() {
		OrderBook ob = new OrderBook();
		Order o1 = new Order(1, 10, 'B', 11);
		ob.addOrder(o1);
		Assertions.assertDoesNotThrow(() -> {
			ob.updateOrderSize(1, 1000);
		});
		Order retrievedOrder = ob.retrieveOrder(1);
		Assertions.assertNotNull(retrievedOrder);
		Assertions.assertEquals(1000, retrievedOrder.getSize());
	}

	@Test
	public void test_getLevelPrice_when_given_invalid_side_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelPrice('d', 1);
		});
	}

	@Test
	public void test_getLevelPrice_when_given_BID_invalid_negative_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelPrice('B', -666);
		});
	}

	@Test
	public void test_getLevelPrice_when_given_BID_invalid_out_of_bounds_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelPrice('B', 16666);
		});
	}

	@Test
	public void test_getLevelPrice_when_given_OFFER_invalid_negative_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelPrice('O', -666);
		});
	}

	@Test
	public void test_getLevelPrice_when_given_OFFER_invalid_out_of_bounds_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelPrice('O', 16666);
		});
	}

	@Test
	public void test_getLevelPrice_shoulReturnOk() {
		OrderBook ob = config1();
		Assertions.assertDoesNotThrow(() -> {
			Double lB1 = ob.getLevelPrice('B', 1);
			Double lB2 = ob.getLevelPrice('B', 2);

			Double lO1 = ob.getLevelPrice('O', 1);
			Assertions.assertNotNull(lB1);
			Assertions.assertEquals(25, lB1);
			Assertions.assertNotNull(lB2);
			Assertions.assertEquals(20, lB2);
			Assertions.assertNotNull(lO1);
			Assertions.assertEquals(20, lO1);
		});
	}

	@Test
	public void test_getLevelTotalSize_when_given_invalid_side_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelTotalSize('Z', 1);
		});
	}

	@Test
	public void test_getLevelTotalSize_when_given_invalid_negative_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelTotalSize('B', -10);
		});
	}

	@Test
	public void test_getLevelTotalSize_when_given_invalid_out_of_bounds_level_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getLevelTotalSize('B', 16666);
		});
	}

	@Test
	public void test_getLevelTotalSize_shoulReturnOk() {
		OrderBook ob = config1();
		Assertions.assertDoesNotThrow(() -> {
			Long totalB1 = ob.getLevelTotalSize('B', 1);
			Long totalB2 = ob.getLevelTotalSize('B', 2);
			Long totalB3 = ob.getLevelTotalSize('B', 3);

			Long totalO1 = ob.getLevelTotalSize('O', 1);
			Assertions.assertNotNull(totalB1);
			Assertions.assertEquals(55, totalB1);
			Assertions.assertNotNull(totalB2);
			Assertions.assertEquals(44, totalB2);
			Assertions.assertNotNull(totalB3);
			Assertions.assertEquals(233, totalB3);
			Assertions.assertNotNull(totalO1);
			Assertions.assertEquals(3, totalO1);
		});
	}

	@Test
	public void test_getAllOrdersBySide_when_given_invalid_side_shouldThrowException() {
		OrderBook ob = config1();
		Assertions.assertThrows(com.test.orders.books.exceptions.InvalidParameterException.class, () -> {
			ob.getAllOrdersBySide('Z');
		});
	}

	@Test
	public void test_getAllOrdersBySide_when_given_config1_should_return_orders_() {
		OrderBook ob = config1();
		Assertions.assertDoesNotThrow(() -> {
			List<Order> bidOrders = ob.getAllOrdersBySide('B');
			List<Order> offerOrders = ob.getAllOrdersBySide('O');

			Assertions.assertNotNull(bidOrders);
			Assertions.assertEquals(4, bidOrders.size());
			Assertions.assertEquals(21, bidOrders.get(0).getId());
			Assertions.assertEquals(20, bidOrders.get(1).getId());
			Assertions.assertEquals(1, bidOrders.get(2).getId());
			Assertions.assertEquals(2, bidOrders.get(3).getId());

			Assertions.assertNotNull(offerOrders);
			Assertions.assertEquals(2, offerOrders.size());
			Assertions.assertEquals(3, offerOrders.get(0).getId());
			Assertions.assertEquals(4, offerOrders.get(1).getId());
		});
	}

	private OrderBook config1() {
		OrderBook ob = new OrderBook();
		ob.addOrder(new Order(1, 10, 'B', 11));
		ob.addOrder(new Order(2, 10, 'B', 222));
		ob.addOrder(new Order(20, 20, 'B', 44));
		ob.addOrder(new Order(21, 25, 'B', 55));

		ob.addOrder(new Order(3, 20, 'O', 3));
		ob.addOrder(new Order(4, 25, 'O', 35));
		return ob;
	}
}
