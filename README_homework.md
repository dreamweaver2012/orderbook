#  OrderBook
@author Dragos-Costin Manea


Pre-requisites:

The project uses Java17+ (required for the dependencies) and Maven.
For additional infos on how to install these tools follow the instructions from the following resources:
Java: https://docs.oracle.com/
Maven: https://maven.apache.org/install.html

After you have installed them you can
1. Build the project by running the following command in the console, opened in the project's root folder:
mvn clean install
In the "target" folder you should have an artifact called "BookOrders-0.0.1-SNAPSHOT.jar".

2. Run the project.
After building it, you can navigate to the subfolder called "target" and run it manually by issuing the following command in the console:
java -jar BookOrders-0.0.1-SNAPSHOT.jar
For the moment the application does nothing!

3. Run the tests with the following command in the root folder: 
mvn test

Alternatively, you can import the project in your favorite IDE (it was developed in Eclipse), using the option to import as a Maven Project.
Then, you can create a Run config where to supply the appropriate JDK or you can use the IDE's terminal to proceed as above.

Project components (classes):
A. Implementation 
- Order - The ”POJO”
- OrderBookContract - Interface that defines the requirements as methods.
- OrderBook - The class implementing the OrderBookContract interface
- InvalidParameterException - Custom exception that encapsulates the context & description when an invalid param is provided to the methods from the OrderBook class.

B. Test classes
- OrderBookTest - Unit tests for the OrderBook class. Almost each method have at least 2 tests: one for the happy flow and one for invalid params provided.

The implementation uses:
- An order registry (map), each entry having as key the order's ID and as value the corresponding Order.
- A price map, each entry having - as key a String composed by concatenating the side type + price value;
								 - as value a list of the orders IDs of the same side and the same price. The IDs are added in the order they were introduced in the system.
- An ordered list of prices for Bids, sorted ascending, by the way it is constructed at each step. It is read from the end to the beginning when used.							 
- An ordered list of prices for Offers, sorted ascending, by the way it is constructed at each step.

Adding or removing an order synchronizes the content of these collections.


Improvements to support real-life, latency-sensitive trading operations:
1. Because execution time is a critical aspect of the trading operations, Java applications can have a drawback compared to applications written in C. The Garbage Collection can ”freeze” or slow down the application threads when freeing massive chunks of memory. 
In order to minimize that we can try to avoid object creation and re-use the existing objects that have to be disposed of. In our case the removed Orders, if deleting Orders happens very often. Instead of physically removing the orders from the OrderBook we can mark them as re-usable (an additional field or we can use some pre-established values for the side (other character than 'B' and 'O') or size (maybe a negative value?) fields to mark this.
2. Use JVMs such as Azul Zing JVM or others that are using different algorithms for GC and reduce drastically the latency by using pre-compiled code for the production platforms by using pre-defined profiles.